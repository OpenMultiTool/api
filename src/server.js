import express from 'express'
import db from './getDb.js'
const app =  express()
app.use(express.json())
app.use(express.urlencoded({extended:true}))
var data = {
    __comment: "This is TEST data it is deprecated and this server is running production API at the moment."
}
app.set('json spaces', 20);
//db.run('INSERT INTO user')

app.get("/", async (req, res)=>{
    res.setHeader("X-Powered-By", "PHP/5.0")
    res.json(data)
})

app.post("/api/users", async (req, res)=>{
    var data = {
        email: req.body.email,
        password: req.body.password
    }
    console.log(JSON.stringify(data))
    var query = 'SELECT * FROM users WHERE (email==?) AND (password==?)'
    db.all(query, [data.email, data.password], (err, result)=>{
        if(result.length==0){
            res.sendStatus(403)
            console.log("Forbidden: " + data.email)
        }else{
            res.sendStatus(200)
            console.log("Success: " + data.email)
        }    
    })
})

app.put("/api/users", (req, res) =>{
    console.log('Got body:', req.body);
    var query = 'INSERT INTO users (name,email,password) VALUES (?,?,?)'
    db.run(query, [req.body.username, req.body.email, req.body.password])
    res.sendStatus(201)
})
app.delete("/api/users", async (req, res)=>{
    var password = req.body.password
    var email = req.body.email 
    var data = [
        email = email,
        password = password 
    ]
    db.all('SELECT * FROM users WHERE email=? AND password=?', [data.email, data.password], (err, result)=>{
        if (result.length==1) {
            db.run('DELETE FROM users WHERE email=? AND password=?', [data.email, data.password], (err)=>{
                res.sendStatus(202)
                console.log("Deleted with body: "+JSON.stringify(data))
            })
        }else{
            res.sendStatus(406)
            console.log("Does not exist")
        }
    })
})

app.listen(process.env.PORT, ()=>{

})